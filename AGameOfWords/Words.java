
public class Words {
	
	Word[] theWord = new Word[25];
	public boolean[] used = new boolean[25];
	public Words()
	{
		super();
	}
	
	public void buildList(){ //defacto setter
		for (int idx=0;idx<25;idx++){
			theWord[idx] = new Word();		
		}
		
		theWord[0].setWord("noodle");
		theWord[1].setWord("poodle");
	    theWord[2].setWord("gamer");
		theWord[3].setWord("doodle");
		theWord[4].setWord("frog");
		theWord[5].setWord("create");
		theWord[6].setWord("instance");
		theWord[7].setWord("falcon");
		theWord[8].setWord("punch");
		theWord[9].setWord("kick");
		theWord[10].setWord("flight");
		theWord[11].setWord("broken");
		theWord[12].setWord("maker");
		theWord[13].setWord("taker");
		theWord[14].setWord("thief");
		theWord[15].setWord("dark");
		theWord[16].setWord("project");
		theWord[17].setWord("carp");
		theWord[18].setWord("muskellunge");
		theWord[19].setWord("pike");
		theWord[20].setWord("sunfish");
		theWord[21].setWord("catfish");
		theWord[22].setWord("bass");
		theWord[23].setWord("major");
		theWord[24].setWord("rampage");
		for (int idx = 0; idx < 25; idx++){
			used[idx] = false;
		}
		
	}
	
	Word getArrayAtIndex(int index){
		return theWord[index];
	}
	
	String getWordAtIndex(int index)
	{
		return theWord[index].getWord();
	}
	
	int getSizeAtIndex(int index)
	{
		return theWord[index].getSize();
	}
	
	void setWasIUsed(int indexOfChosen,boolean tof){
		used[indexOfChosen] = tof;
	}
	
	boolean getWasIused(int indexOfChosen){
		return used[indexOfChosen];
	}
	
	
}
