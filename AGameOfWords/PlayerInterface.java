import java.util.Scanner;

public class PlayerInterface {
	
	Scanner user_input = new Scanner( System.in );
	
	char getChar(){
		char c = user_input.next().charAt(0);
		return c;
	}
	
	String getString(){
		return user_input.nextLine();
	}
}
