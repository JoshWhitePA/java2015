
import java.util.ArrayList;
import java.util.Arrays;

public class Select {
	
	Word theChosenWord;
	String theBlankedWord;
	RandomInterger randy = new RandomInterger();
	Words theChosen = new Words();
	int[] charRemoveArray;
	int arrayLength;
	char[] theCharAt;
//===============================================================================
	void selectWord(){//the word has to be selected before remove letters from the word
		int sRNumber;
		theChosen.buildList();
		sRNumber = randy.randomRange(0,24);// gets random number within range and set it to sRNumber 
		theChosenWord = theChosen.getArrayAtIndex(sRNumber);//uses function from Words, 
	}
	
//===============================================================================
	Word getTheChosen(){
		return theChosenWord;
	}
//===================================================================================	
	int lettersToRemove(){
		
		//Word theChosen = new Word();
		theChosenWord.getSize();
		int sRNumber;
		String fullyBlanked = theChosenWord.getWord();
		sRNumber = randy.randomRange(2,theChosenWord.getSize()-2);
		int numToRemove = sRNumber;
		int[] removedChar = new int[sRNumber];
		
		for (int numRem=0; numRem != numToRemove; numRem++){ //fills array that was made to size specified by random number.
			removedChar[numRem] =  randy.randomRange(0,theChosenWord.getSize()-1);//chooses position of character to be removed
		}
		
		Arrays.sort(removedChar);
		removedChar = removeDuplicates(removedChar);
		setArrayLength(removedChar.length);
		
		for (int numRem=0;numRem != getArrayLength(); numRem++){
			  //theCharAt[numRem] = fullyBlanked.charAt(numRem);
			fullyBlanked=replaceCharAt(fullyBlanked, removedChar[numRem], '_');
		}
//========================================		
setTheBlankedWord(fullyBlanked);

		return sRNumber;
	} 
	
//=================================================================================	
	//Set of blanked word
void setTheBlankedWord(String theBlanked){
		theBlankedWord = theBlanked;
	}
//==================================================================================
	//get of blanked word
	String getTheBlankedWord(){
		return theBlankedWord;
	}
//=================================================================================	
	public static String replaceCharAt(String s, int pos, char c) {
	    return s.substring(0, pos) + c + s.substring(pos + 1);
	  }
	
	public int[] removeDuplicates(int[] input){
		int j = 0;
		int i = 1;
		//return if the array length is less than 2
		if(input.length < 2){
			return input;
		}
		while(i < input.length){
			if(input[i] == input[j]){
				i++;
			}
			else{
				input[++j] = input[i++];
			}   
		}
		int[] output = new int[j+1];
		for(int k=0; k<output.length; k++){
			output[k] = input[k];
		}
		return output;
	}
	
	
	void setArrayLength(int array){
		arrayLength = array;
	}
	
	
	int getArrayLength(){
		return arrayLength;
	}
	
	ArrayList<Integer> indexOfBlanks(){
		 String string = getTheBlankedWord();
		 ArrayList<Integer> list = new ArrayList<Integer>();
		 char character = '_';
		 for(int i = 0; i < string.length(); i++){
		     if(string.charAt(i) == character){
		        list.add(i);
		     }
		 }
		return list;
	}
	
}
