
public class Display {
	
	public void prompt(String mainPrompt){
		System.out.print(mainPrompt + ": ");
	}
	
	public void displayPuzzle(String disp) {
		//Puzzle kickOff = new Puzzle();
		System.out.println(/*kickOff.blankedWordSetup()*/disp+"\n");
	  }

	  public void displaySolution() {
		  System.out.println("");
	  }

	  public void displayInstructions() {
	      System.out.println("Lets Play A Game...");
	      System.out.println("The game is played in rounds.");
	      System.out.println("In each round the you is presented with a word puzzle.");
	      System.out.println("A word puzzle consists of a word with some of its letters removed.");
	      System.out.println("The player will guess the letters that completes the word.");
	      System.out.println("Type each letter and hit enter to submit the letter");
	      System.out.println("After all letters have been entered you will be told if the word was correct");
	      System.out.println("You will have three tries before the word is counted as wrong");
	  }

	  public void displayError(String message) {
		  System.out.print(message+"\n"); 
	  }

	  public void updatePuzzle() {
	  }

	  public void displayLetterWrong(){
		  System.out.println("Sorry, you answered wrong");
	  }
	  
	  public void displayWordWrong(){
		  System.out.println("Sorry, you got the word wrong");
	  }
	  
	  public void message(String message){
		  System.out.println(message);
	  }
}
