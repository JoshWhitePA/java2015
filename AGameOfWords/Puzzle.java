import java.util.ArrayList;


public class Puzzle {
	
	//Select select = new Select();
	PlayerInterface UI = new PlayerInterface();
	Display display = new Display();
	PuzzleScore score = new PuzzleScore();
	int wrongTries=0;
	
	String blankedWordSetup(Select select){
		int numLettersToRemove;
		select.selectWord();
		numLettersToRemove = select.lettersToRemove();// could make function void because I don't use return, could 
		return select.getTheBlankedWord();
	}
	
	boolean checkCorrect (int index, char answer,Select select){
		ArrayList<Integer> array = select.indexOfBlanks();
		int arraySize = array.size();
		int[] amy;
		//toArray(array);
		String correctWord = select.theChosenWord.getWord();
		//System.out.println("right word :"+correctWord+" index used: " + index + " char at index: "+ correctWord.charAt(array.get(index))+ " = "+ answer);
		//System.out.println("\n"+ array.get(index));
		if (answer == correctWord.charAt(array.get(index))){//this needs to check index located at actual index not fake one
			return true;
		}
		else {
			
			return false;
		}
	}
	
	boolean nextBlank(Select select){
		ArrayList<Integer> array = select.indexOfBlanks();
		int arraySize = array.size();
		int count= 0;
		boolean amICorrect = false, flag = false;
		char response;
		boolean[] tof = new boolean[array.size()];
		
		int outCount = 0;
		if ( getWrongTries() < 3){
			display.displayPuzzle(select.getTheBlankedWord());
			while (arraySize > count){
				display.prompt("Guess for possition "+(count+1));
				response = UI.getChar();
				amICorrect = checkCorrect(count,response,select);
				tof[count]=amICorrect;
				count++;
			  }
			count = 0;//count reset
			int numTrue = 0;//stores number of true answers there were
			int numFalse = 0;
			while (arraySize > count){
				if (tof[count] == true){
					numTrue++;
					count++;
				}//end inner if1
				else{
					numFalse++;
					count++;
				}//end inner else1
			}//end inner while1
			count =0;
			if (numTrue == array.size()) {// if all are true then it breaks out of the for loop and updates the score
				System.out.println("in "+numTrue);
				score.incrementScore(select.getTheBlankedWord());//is passed string and length is used to increment
				flag = true;
				return tryAgain("You got it right!");
				//break;
			}//end of inner if2
			else{
				score.decrimentScore(select.getTheBlankedWord());
				display.message("Sorry, your guesses were wrong for this word. Your can try "+((getWrongTries()*-1)+2)+" more words.");
				display.message("The word was "+select.getTheChosen().getWord());
				plusEqualWrongTries();
				numTrue =0;
				numFalse = 0;
				if (getWrongTries() < 3){
					return tryAgain("");
				}
				display.message("Game Over, you have gotten too many words wrong");
				display.message("Your score was "+ score.getScore());
				return false;
			}
			//outCount++;
		}//end of if 
		return false;//again, eclipse doesn't like me only returning in if's check here if returns are weird
	}
	
	
	boolean tryAgain(String message){
		display.message(message);
		char again = 't';
		while (again != 'n' && again != 'N' &&again != 'y' &&again != 'Y'){
			display.message("Your Current Score Is "+score.getScore());
			display.prompt("Would you like another word?(Y/Yes,N/No)");
			again = UI.getChar();
		}
		if (again == 'y'|| again == 'Y')
			return true;
		else if (again =='N' || again == 'n'){
			display.message("Your score was "+ score.getScore());
			return false;
		}
		return true;//should not be triggered but eclipse didn't like returns in if's
		}
	
	void plusEqualWrongTries(){
		wrongTries += 1;
	}
	
	void setWrongTries(int wrong){
		wrongTries = wrong;
	}
	
	int getWrongTries(){
		return wrongTries;
	}
	
}
