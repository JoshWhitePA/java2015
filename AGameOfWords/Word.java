//import java.io.*;

public class Word {

	private String word;
	private int size;
	private boolean used = false;
	
	public Word (){
		word ="";
		size = 0;
	}
	
	public Word (String aWord,int aSize)
	{
		word = aWord;
		size = aWord.length();
	}
	
	public void setWord(String aWord){	
		word = aWord;
		size = word.length();
	}
	
	public String getWord(){
		return word;
	}
	
	public void setSize(int theSize){//sets the size of the string word
		size = theSize;
	}
	
	public int getSize(){//gets the size of the string word
		return size;
	}
	
	public void setUsed (boolean wasIUsed){
		used = wasIUsed;
	}
	
	public boolean getUsed(){
		return used;
	}
	
	
}
